<?php
declare(strict_types=1);

namespace Mrx\Core\JsonRpc\User;

use Mrx\Core\Dto\User\MemberDto;

/**
 * 用户服务相关接口
 */
interface UserServiceInterface
{
    /**
     * @Desc:通过用户id获取用户信息
     * @Funtion: getUserInfoByUserId
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @param int $userId
     * @return mixed
     */
    public function getUserInfoByUserId(int $userId);

    /**
     * @Desc:通过用户名获取用户信息
     * @Funtion: getUserInfoByUserName
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @param string $userName
     * @return mixed
     */
    public function getUserInfoByUserName(string $userName);

    /**
     * @Desc:用户注册
     * @Funtion: register
     * @Author: mrx
     * @CreateTime: 2023-07-13
     * @param MemberDto $dto
     * @return mixed
     */
    public function register(MemberDto $dto);
}