<?php

namespace Mrx\Core\Mapper;

use Mrx\Core\Mapper\Traits\MapperTrait;
use Mrx\Core\Model\BaseModel;

/**
 * @Desc:仓库--抽象基类
 * @Class: AbstractMapper
 * @Package: Mrx\Core\Mapper
 * @Author: mrx
 * @CreateTime: 2023-07-12  16:05
 * @Version: 1.0
 */
abstract class AbstractMapper
{
    use MapperTrait;

    /**
     * @var BaseModel
     */
    public $model;

    /**
     * @Desc:设置当前模型
     * @Funtion: assignModel
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @return mixed
     */
    abstract public function assignModel();

    /**
     * 初始胡构造函数
     */
    public function __construct()
    {
        $this->assignModel();
    }
}