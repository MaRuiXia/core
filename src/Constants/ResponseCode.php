<?php
declare(strict_types=1);

namespace Mrx\Core\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Desc:响应码-- 枚举类
 * @Class: ResponseCode
 * @Package: Mrx\Core\Constants
 * @Author: mrx
 * @CreateTime: 2023-07-04  11:44
 * @Version: 1.0
 */
#[Constants]
class ResponseCode extends AbstractConstants
{
    /**
     * @Message("success")
     */
    const SUCCESS = 200;

    /**
     * @Message("创建成功");
     */
    const CREATE_ED = 201;

    /**
     * @Message("更新成功");
     */
    const UPDAATE_ED = 202;

    /**
     * @Message("Server Error！")
     */
    const SERVER_ERROR = 500;

    /**
     * @Message("参数校验错误！")
     */
    const VALIDATOR_ERROR = 4000;

    /**
     * @Message("未授权！")
     */
    const UNAUTHORIZED = 4001;

    /**
     * @Message("参数错误")
     */
    const PARAMS_ERROR = 4002;

    /**
     * 服务器已经接受到请求，但拒绝执行
     * @Message("FORBIDDEN");
     */
    const FORBIDDEN = 4003;

    /**
     * @Message("资源不存在")
     */
    const RESOURE_NO_FOUND = 4004;

    /**
     * @Message("请求频繁")
     */
    const MAX_REQUEST = 4029;

    /**
     * RPC调用失败
     * @Message("RPC调用失败");
     */
    const  RPC_ERROR = 4300;

    /**
     * @Message("缺少token")
     */
    const INVALID_TOKEN = 1000;

    /**
     * @Message("token过期")
     */
    const TOKEN_EXPIRED = 1002;

    /**
     * @Message("验证码无效")
     */
    const MSG_CODE_ERROR = 1004;

    /**
     * @Message("验证码过期")
     */
    const MSG_CODE_EXPIRED = 1005;

    /**
     * @Message("用户不存在")
     */
    const USER_NOT_EXIST = 1101;

    /**
     * @Message("用户已存在")
     */
    const USER_IS_EXIST = 1102;

    /**
     * @Message("密码错误")
     */
    const USER_PWD_ERROR = 1103;

    /**
     * @Message("签名错误")
     */
    const SIGN_ERROR = 1201;
}