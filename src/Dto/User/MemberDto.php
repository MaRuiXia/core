<?php

namespace Mrx\Core\Dto\User;

use Mrx\Core\Dto\AbstractBaseDto;

/**
 * @Desc:用户接口相关值对象
 * @Class: MemberDto
 * @Package: Mrx\Core\Dto\User
 * @Author: mrx
 * @CreateTime: 2023-07-13  14:21
 * @Version: 1.0
 */
class MemberDto extends AbstractBaseDto
{
    protected $fillable = [
        'username',
        'mobile',
    ];

    public function getData()
    {
        return [
            'username' => $this->getItem('username'),
            'mobile' => $this->getItem('mobile')
        ];
    }
}