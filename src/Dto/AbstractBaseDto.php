<?php

namespace Mrx\Core\Dto;

use Cblink\Dto\Dto;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Desc:DTO抽象基类
 * @Class: AbstractBaseDto
 * @Package: Mrx\Core\Dto
 * @Author: mrx
 * @CreateTime: 2023-07-13  10:38
 * @Version: 1.0
 */
abstract class AbstractBaseDto extends Dto
{
    public function __construct($data = null)
    {
        if (!is_array($data)) {
            $data = make(RequestInterface::class)->all();
        }

        parent::__construct($data);
    }
}