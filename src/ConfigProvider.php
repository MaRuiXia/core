<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Mrx\Core;

use Hyperf\Serializer\Serializer;
use Hyperf\Serializer\SerializerFactory;
use Mrx\Core\Model\Visitor\ModelUpdateVisitor;
use Mrx\Core\Exception\Handler\AppExceptionHandler;
use Mrx\Core\Exception\Handler\BusinessExceptionHandler;
use Mrx\Core\Exception\Handler\JsonRpcExceptionHandler;
class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                Hyperf\Contract\NormalizerInterface::class => new SerializerFactory(Serializer::class),
                Hyperf\Database\Commands\Ast\ModelUpdateVisitor::class => ModelUpdateVisitor::class,
            ],
            'commands' => [
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'exceptions' => [
                'handler' => [
                    'http' => [
                        AppExceptionHandler::class,
                        BusinessExceptionHandler::class,
                    ],
                    'jsonrpc-http' => [
                        JsonRpcExceptionHandler::class,
                    ]
                ],
            ],
        ];
    }
}
