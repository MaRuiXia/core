<?php
declare(strict_types=1);

namespace Mrx\Core\Controller;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\Container\ContainerInterface;
use Mrx\Core\Response\ApiResponse;
/**
 * @Desc:控制器基类
 * @Class: AbstractController
 * @Package: Mrx\Core\Controller
 * @Author: mrx
 * @CreateTime: 2023-07-07  11:25
 * @Version: 1.0
 */
abstract class AbstractController
{
    #[Inject]
    protected ContainerInterface $container;

    #[Inject]
    protected RequestInterface $request;

    #[Inject]
    protected ApiResponse $response;
}