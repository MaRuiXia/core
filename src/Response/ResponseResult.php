<?php

namespace Mrx\Core\Response;

use Mrx\Core\Constants\ResponseCode;

/**
 * 返回结果
 * Created by macro on 2019/4/19.
 */
class ResponseResult
{
    /**
     * @Desc:成功返回
     * @Funtion: success
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @param array $data
     * @return array
     */
    public static function success(array $data = [])
    {
        return self::commonResuls(ResponseCode::SUCCESS, ResponseCode::getMessage(ResponseCode::SUCCESS), $data, true);
    }

    /**
     * @Desc:错误返回
     * @Funtion: error
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @param int $code
     * @param string $message
     * @param array $data
     * @return array
     */
    public static function error(int $code = ResponseCode::SERVER_ERROR, string $message = '', array $data = [], bool $success = false)
    {
        if (empty($message)) {
            return self::commonResuls($code, ResponseCode::getMessage($code), $data, $success);
        } else {
            return self::commonResuls($code, $message, $data, $success);
        }
    }

    /**
     * @Desc:统一返回格式
     * @Funtion: commonResuls
     * @Author: mrx
     * @CreateTime: 2023-07-12
     * @param int $code
     * @param string $message
     * @param array $data
     * @return array
     */
    public static function commonResuls(int $code, string $message, array $data, bool $success)
    {
        return [
            'success' => $success,
            'message' => $message,
            'code' => $code,
            'data' => &$data,
        ];
    }
}