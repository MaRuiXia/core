<?php

namespace Mrx\Core\Model\Visitor;

use Hyperf\Database\Commands\Ast\ModelUpdateVisitor as Visitor;
use Hyperf\Utils\Str;

/**
 * @Desc:gen:model 时转化相关类型为其他类型.
 * @Class: ModelUpdateVisitor
 * @Package: Mrx\Core\Model\Visitor
 * @Author: mrx
 * @CreateTime: 2023-07-11  11:10
 * @Version: 1.0
 */
class ModelUpdateVisitor extends Visitor
{
    /**
     * @param string $type
     * @return string|null
     */
    protected function formatDatabaseType(string $type): ?string
    {
        return match ($type) {
            'tinyint', 'smallint', 'mediumint', 'int', 'bigint' => 'integer',
            'decimal' => 'decimal:2',
            'float', 'double', 'real' => 'float',
            'bool', 'boolean' => 'boolean',
            'json' => 'array',
            default => null,
        };
    }

    /**
     * @param string $type
     * @param string|null $cast
     * @return string|null
     */
    protected function formatPropertyType(string $type, ?string $cast): ?string
    {
        if (! isset($cast)) {
            $cast = $this->formatDatabaseType($type) ?? 'string';
        }

        switch ($cast) {
            case 'integer':
                return 'int';
            case 'date':
            case 'datetime':
                return '\Carbon\Carbon';
            case 'json':
                return 'array';
        }

        if (Str::startsWith($cast, 'decimal')) {
            // 如果 cast 为 decimal，则 @property 改为 string
            return 'string';
        }

        return $cast;
    }
}