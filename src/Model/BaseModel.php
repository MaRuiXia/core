<?php

namespace Mrx\Core\Model;

use Hyperf\DbConnection\Model\Model;
use Hyperf\ModelCache\Cacheable;
use Hyperf\ModelCache\CacheableInterface;

/**
 * @Desc:模型基础类
 * @Class: BaseModel
 * @Package: Mrx\Core\Model
 * @Author: mrx
 * @CreateTime: 2023-07-12  16:09
 * @Version: 1.0
 */
class BaseModel extends Model implements CacheableInterface
{
    use Cacheable;

    /**
     * 默认每页记录数
     */
    public const PAGE_SIZE = 15;
}