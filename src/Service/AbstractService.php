<?php

namespace Mrx\Core\Service;

use Hyperf\Context\Context;
use Mrx\Core\Mapper\AbstractMapper;
use Mrx\Core\Service\Traits\ServiceTrait;

/**
 * @Desc:服务层--抽象基础类
 * @Class: AbstractService
 * @Package: Mrx\Core\Service
 * @Author: mrx
 * @CreateTime: 2023-07-12  16:00
 * @Version: 1.0
 */
abstract class AbstractService
{
    //use ServiceTrait;

    /**
     * 仓库注入
     * @var AbstractMapper
     */
    public $mapper;

    /**
     * 把数据设置为类属性
     * @param array $data
     */
    public function setAttributes(array $data)
    {
        Context::set('attributes', $data);
    }

    /**
     * 魔术方法，从类属性里获取数据
     * @param string $name
     * @return mixed|string
     */
    public function __get(string $name)
    {
        return $this->getAttributes()[$name] ?? '';
    }

    /**
     * 获取数据
     * @return array
     */
    public function getAttributes(): array
    {
        return Context::get('attributes', []);
    }
}