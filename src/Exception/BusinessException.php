<?php

namespace Mrx\Core\Exception;

/**
 * @Desc:业务异常类
 * @Class: BusinessException
 * @Package: Mrx\Core\Exception
 * @Author: mrx
 * @CreateTime: 2023-07-07  16:57
 * @Version: 1.0
 */
class BusinessException extends AbstractException
{

}