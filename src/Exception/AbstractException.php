<?php

namespace Mrx\Core\Exception;

use Hyperf\Server\Exception\ServerException;
use Mrx\Core\Constants\ResponseCode;

/**
 * @Desc:抽象异常类基类
 * @Class: AbstractException
 * @Package: Mrx\Core\Exception
 * @Author: mrx
 * @CreateTime: 2023-07-07  16:55
 * @Version: 1.0
 */
abstract class AbstractException extends ServerException
{
    public function __construct(int $code = 500, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ResponseCode::getMessage($code);
        }

        parent::__construct($message, $code, $previous);
    }
}