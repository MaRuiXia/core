<?php

namespace Mrx\Core\Exception;

/**
 * @Desc:JsonRpc服务异常
 * @Class: JsonRpcException
 * @Package: Mrx\Core\Exception
 * @Author: mrx
 * @CreateTime: 2023-07-13  09:52
 * @Version: 1.0
 */
class JsonRpcException extends AbstractException
{

}